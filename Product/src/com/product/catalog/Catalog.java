package com.product.catalog;

import java.util.Random;

import com.product.Product;
import com.product.products.Lotion;
import com.product.products.Parfume;

public class Catalog {
	
	public Product create_Product() {
		
		Random random = new Random();
		float lotionPrice = 13.4f;
		float parfumePrice = 18.6f;
		
		int low = 100;
		int high = 999;
		int result = random.nextInt(high - low) + low;
		
		int number =  (int) Math.round(Math.random());
		
		if(number == 0) {
			Product product = new Lotion(result, lotionPrice);
			System.out.println(product.getClassName());
		}else if(number == 1) {
			Product product = new Parfume(result, parfumePrice);
			System.out.println(product.getClassName());
		}
		
		
		return null;
	}

}
package com.product;

public class Product {

	private int id;
	private float price;

	public Product(int id, float price) {
		this.id = id;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public String getClassName() {
		return getClass().getSimpleName();
	}
	
	// o metoda sa transforme numarul "id", in P + xxx si LC + xxx. 
	private String getEncrypt() {
		String theEncrypt = "";
		
		if(getClassName().equals("Lotion")) {
			theEncrypt = "LCxxx";
		}else if(getClassName().equals("Parfume")) {
			theEncrypt = "Pxxx";
		}
		
		return theEncrypt;
	}
	
	public String toString() {
		return ("Class name -> " + getClassName() + "; price -> " + getPrice() + "; the id -> " + getEncrypt());
	}

}